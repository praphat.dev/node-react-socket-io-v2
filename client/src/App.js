import './App.css';
import { useState, useEffect } from 'react';
import axios from 'axios';
import io from 'socket.io-client';

function App() {
  const [username, setUsername] = useState('');
  const [signIn, setupSignIn] = useState(false);

  const handleLogin = (e) => {
    e.preventDefault();
    console.log('Logging in with', username);
    // call api sign ins
    const option = {
      method: 'POST',
      url: 'http://localhost:3002/users/sign-in',
      responseType: 'json',
      charset: 'utf8',
      responseEncodig: 'utf8',
      data: {
        username
      },
      headers: {
        'Content-Type': 'application/json'
      }
    };
    axios(option)
      .then((res) => {
        console.log(res.data);
        setUsername(username);
        setupSignIn(true);
        checkAuth(username);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleLogout = (e) => {
    e.preventDefault();
    console.log('Logging out with', username);
    // call api sign out
    const option = {
      method: 'POST',
      url: 'http://localhost:3002/users/sign-out',
      responseType: 'json',
      charset: 'utf8',
      responseEncodig: 'utf8',
      data: {
        username
      },
      headers: {
        'Content-Type': 'application/json'
      }
    };
    axios(option)
      .then((res) => {
        console.log(res.data);
        setUsername('');
        setupSignIn(false);
        checkAuth(username);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const checkAuth = (username) => {
    const socket = io('http://localhost:3002', {
      transports: ['websocket', 'polling']
    });
    socket.on(`auth-${username}`, (data) => {
      console.log('Auth', data);
      if (!data) {
        setUsername('');
        setupSignIn(false);
      }
    });

    return () => {
      // socket off
      socket.off(`auth-${username}`);

      socket.disconnect();
    };
  };

  useEffect(() => {
    // console.log('useEffect');
    // // setup socket.io sd dzxfdas sasas
    // const socket = io('http://localhost:3002', {
    //   transports: ['websocket', 'polling']
    // });
    // console.log(socket);
    // socket.on(`auth-${username}`, (data) => {
    //   console.log(`auth-${username}`);
    //   console.log('Auth', data);
    //   if (!data) {
    //     setUsername('');
    //     setupSignIn(false);
    //   }
    // });
    // return () => {
    //   // socket off
    //   socket.off(`auth-${username}`);
    //   socket.disconnect();
    // };
  }, []);

  return (
    <div className="App">
      {/* if username show dashboard or show sign in*/}
      {signIn ? (
        <div>
          <h1>
            Welcome <p style={{ color: 'red' }}>{username}</p>
          </h1>
          <button onClick={handleLogout}>Sign Out</button>
        </div>
      ) : (
        <>
          <h1>Sign In</h1>
          <form onSubmit={handleLogin}>
            <input
              type="text"
              placeholder="Enter your username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <button type="submit">Sign In</button>
          </form>
        </>
      )}
    </div>
  );
}

export default App;
