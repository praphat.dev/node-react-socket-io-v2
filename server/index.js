const express = require('express');
const { createServer } = require('http');
const { Server } = require('socket.io');
const { createAdapter } = require('@socket.io/cluster-adapter');
const { setupWorker } = require('@socket.io/sticky');

// Initialize Express app and create HTTP server
const app = express();

// Cors middleware
const cors = require('cors');
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const httpServer = createServer(app);
const io = new Server(httpServer);

io.adapter(createAdapter());

setupWorker(io);

// Store connected users
const connectedUsers = new Set();

io.on('connection', (socket) => {
  console.log(`connect ${socket.id}`);

  connectedUsers.add(socket);

  // Listen for disconnect event
  socket.on('disconnect', () => {
    console.log('User disconnected');
    // Remove the user from the connected users set
    connectedUsers.delete(socket);
  });
});

// Route for user sign-in
app.post('/users/sign-in', (req, res) => {
  const { username } = req.body;
  console.log(`User signed in ${username}`);

  // emit the user sign-in event to all connected sockets running on pm2 cluster

  io.emit(`auth-${username}`, true);

  res.sendStatus(200);
});

// Route for user sign-out
app.post('/users/sign-out', (req, res) => {
  const { username } = req.body;
  console.log('signing out', username);
  // Emit event to all connected sockets

  io.emit(`auth-${username}`, false);

  res.sendStatus(200);
});

const PORT = process.env.PORT || 3002;
httpServer.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
