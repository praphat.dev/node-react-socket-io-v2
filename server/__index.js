// Import required modules
const express = require('express');
const http = require('http');
const socketIo = require('socket.io');

// Initialize Express app and create HTTP server
const app = express();
const server = http.createServer(app);

// Cors middleware
const cors = require('cors');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Initialize Socket.IO
const io = socketIo(server);

// Store connected users
const connectedUsers = new Set();

// Socket.IO connection event
io.on('connection', (socket) => {
  console.log('A user connected');

  // Add the user to the connected users set
  connectedUsers.add(socket);

  // Listen for disconnect event
  socket.on('disconnect', () => {
    console.log('User disconnected');
    // Remove the user from the connected users set
    connectedUsers.delete(socket);
  });
});

// Route for user sign-in
app.post('/users/sign-in', (req, res) => {
  const { username } = req.body;

  // Emit event to all connected sockets
  io.emit(`auth-${username}`, true);

  res.sendStatus(200);
});

// Route for user sign-out
app.post('/users/sign-out', (req, res) => {
  const { username } = req.body;
  console.log('signing out', username);
  // Emit event to all connected sockets
  io.emit(`auth-${username}`, false);

  res.sendStatus(200);
});

// Route for getting auth-${username} values

// Start the server
const PORT = process.env.PORT || 3002;
server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
