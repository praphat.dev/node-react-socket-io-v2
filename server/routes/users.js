const express = require('express');
const router = express.Router();

router.post('/sign-in', async (req, res) => {
  const { username } = req.body;
  if (!username) {
    res.status(400).json({ error: 'username is required' });
  }

  // create a new socket.io connection
  req.app.io.on('connection', (socket) => {
    console.log('a user connected');

    req.app.io.emit(`auth`, true);

    socket.on('disconnect', () => {
      console.log('user disconnected');
    });
  });

  res.status(200).json({ data: username });
});

router.post('/sign-out', (req, res) => {
  const { username } = req.body;

  if (!username) {
    res.status(400).json({ error: 'username is required' });
  }

  // setup socket.io signout username

  res.status(200).json({ username });
});

module.exports = router;
